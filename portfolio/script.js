const name = document.querySelector("#myName");
const char_array = [
  "A_",
  "Ak_",
  "Aks_",
  "Aksh_",
  "Akshi_",
  "Akshit _",
  "Akshit S_",
  "Akshit Si_",
  "Akshit Sin_",
  "Akshit Sing_",
  "Akshit Singh_",
  "Akshit Singha_",
  "Akshit Singhal",
  "Akshit Singhal ❤️",
];

let index = 0;
let traversed = false;
function movingName() {
  let newName = char_array[index];
  if (!traversed) {
    name.innerText = newName;
    index++;
    if (index === char_array.length - 1) {
      traversed = true;
    }
  } else {
    index--;
    name.innerText = newName;
    if (index === 0) {
      traversed = false;
    }
  }
}

setInterval(movingName, 500);

const body = document.querySelector('body');
const toggle = document.querySelector('#toggle');
const sun = document.querySelector('.toggle .bxs-sun');
const moon = document.querySelector('.toggle .bx-moon');

toggle.addEventListener('change', () => {
    body.classList.toggle("dark");
})
